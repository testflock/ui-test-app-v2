package test_utils;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import test_utils.common.Property;
import test_utils.common.Wait;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static test_utils.ExtentReports.ExtentTestManager.endTest;

/**
 * Created by vpetrou on 1/3/2018.
 */
public class BaseTestCase {

    public WebDriver driver;
    public Page page;
    public ExtentTest extentTest;

    public static final Map<String, String> TC_Desc;

    static {
        final Map<String, String> tc_desc = new HashMap<>();
        tc_desc.put("TC_UI_Login_001", "Login with Correct Credentials");
        tc_desc.put("TC_UI_Login_002", "Logout after correct login");
        tc_desc.put("TC_UI_N_Login_001", "Login with Null Credentials");
        tc_desc.put("TC_UI_Contact_001", "Add a new contact");
        tc_desc.put("TC_UI_Contact_002", "View a contact");
        tc_desc.put("TC_UI_Contact_003", "Edit contact");
        tc_desc.put("TC_UI_Contact_004", "Delete a contact");
        TC_Desc = Collections.unmodifiableMap(tc_desc);
    }

    @BeforeTest
    public void beforeTest() {
        extentTest = new ExtentTest(getTestCaseId(), TC_Desc.get(getTestCaseId()));
        //OPEN BROWSER AND APPLICATION URL
        System.out.println("browser:" + Property.getBrowser());
        if (Property.getBrowser().equalsIgnoreCase("chrome")) {

            //get file according to OS
            String webdriverFileName = "";
            if (Property.getApplicationOS().equalsIgnoreCase("mac")) {
                webdriverFileName = "chromedriver_mac";
            } else {
                webdriverFileName = "chromedriver.exe";
            }

            // get driverPath. path is different when running from IntelliJ and different when running from deployed package
            String driverPath = Property.getApplicationPath() + "/src/main/resources/drivers/" + webdriverFileName;
            File f = new File(driverPath);
            if (!f.exists()) {
                driverPath = Property.getApplicationPath() + "/drivers/" + webdriverFileName;
                File extentDir = new File(Property.getApplicationPath() + "/extent-reports");
                extentDir.mkdir();
            }

            // set the location of chrome driver file (resources)
            System.setProperty("webdriver.chrome.driver", driverPath);
            // create a new driver (chrome driver)
            driver = new ChromeDriver();
        }
        //only for mac that supports many Desktops
        if (Property.getApplicationOS().equalsIgnoreCase("mac")) {
            focusBrowser(driver);
        }
        // open web application
        driver.get(Property.getURL());
        //maximize window
        driver.manage().window().maximize();

        page = new Page(driver);

        // wait for page to load (because sometime we put the correct element locators but unexpectedly script fails.)
        Wait.forPageToLoad(driver);
    }

    @AfterTest
    public void afterTest() throws InterruptedException {
        driver.quit();
        endTest();
    }

    public WebDriver getDriver() {
        return driver;
    }

    private void focusBrowser(WebDriver driver) {
        String currentWindowHandle;
        currentWindowHandle = driver.getWindowHandle();

        ((JavascriptExecutor) driver).executeScript("alert('testFlock.org:7101')");
        driver.switchTo().alert().accept();

        driver.switchTo().window(currentWindowHandle);

    }

    protected String getTestCaseId() {
        return this.getClass().getSimpleName().toString();
    }

}
