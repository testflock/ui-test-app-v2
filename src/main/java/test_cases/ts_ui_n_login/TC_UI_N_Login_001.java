package test_cases.ts_ui_n_login;

import org.testng.annotations.Test;
import test_utils.BaseTestCase;

import static test_utils.ExtentReports.ExtentTestManager.getTest;

/**
 * Created by vpetrou on 2/3/2018.
 */
public class TC_UI_N_Login_001 extends BaseTestCase {

    @Test
    public void TC_UI_N_Login_001() {
        getTest().setDescription(TC_Desc.get(getTestCaseId()));
        page.loginPage()
                .clickLogin()
                .verifyInvalidLogin();
    }

}
