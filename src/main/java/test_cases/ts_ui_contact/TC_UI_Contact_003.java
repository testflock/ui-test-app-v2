package test_cases.ts_ui_contact;

import org.testng.annotations.Test;
import test_utils.BaseTestCase;
import test_utils.ExtentReports.ExtentTestManager;

import static test_utils.ExtentReports.ExtentTestManager.getTest;

/**
 * Created by vpetrou on 1/3/2018.
 */
public class TC_UI_Contact_003 extends BaseTestCase {

    @Test
    public void TC_UI_Contact_003() {
        getTest().setDescription(TC_Desc.get(getTestCaseId()));
        page.loginPage()
                .login("bill@testflock.org", "1234");
        page.homePage()
                .verifyPageOpens()
                .verifyLoggedUser("bill@testflock.org");
        page.menu()
                .goToListOfContacts();
        page.listOfContactsPage()
                .clickDetailsOf("Bill");
        page.viewContactPage()
                .verifyViewMode()
                .clickEdit();
        page.updateContactPage()
                .verifyPageOpens()
                .updateCity("Thessaloniki")
                .updateAddress("Makedonias 150")
                .clickUpdate();
        page.viewContactPage()
                .verifyViewMode()
                .verifyViewListContains("Thessaloniki")
                .verifyViewListContains("Makedonias 150")
                .clickBack();
        page.listOfContactsPage()
                .verifyTableContains("Thessaloniki");
        page.menu()
                .logout();
    }

}
